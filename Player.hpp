#ifndef PLAYER_H
#define PLAYER_H

#include <Arduino.h>
#include <FastLED.h>


class Player {
    private:
        byte button_pin;
        byte button_state;
        int points_to_win;
        int points;
    public:
        CRGB color;
        byte position;
        byte hitting_range;
        short direction;
        bool won;
        Player(byte pin, CRGB color, byte position, byte hitting_range, int points_to_win);
        void init();
        bool pressing();
        int hitting(byte ball_position);
        void score();
};

#endif
