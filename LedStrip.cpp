#include "LedStrip.hpp"


void Strip::set_brightness(int value) {
    brightness = map(value, 0, 1023, 0, 128);
    FastLED.setBrightness(brightness);
}


void Strip::set_random_color_all() {
    for(int i=0; i < num_leds; i++) {
        set_random_color_led((i + 1) % num_leds);
    }
}

void Strip::turn_on(CRGB color, int start, int end) {
    for(int i=start; i < end; i++) {
        leds[i] = color;
    }
}

void Strip::turn_all_off() {
   turn_on(CRGB::Black, 0, num_leds);
}

void Strip::turn_all_on(CRGB color) {
   turn_on(color, 0, num_leds);
}

void Strip::set_random_color_led(int i) {
        leds[i].red = random(255);
        leds[i].blue = random(255);
        leds[i].green = random(255 - 100);
}

void Strip::set_christmas_color_led(int i) {
        leds[i].red = random(255);
        leds[i].blue = 0;
        leds[i].green = random(255 - 100);
}

bool Strip::check_time(int ms_delay) {
    current_time = millis();
    if(current_time - previous_time < ms_delay)
        return false;
    previous_time = current_time;
    return true;
}

void Strip::win_animation(CRGB color) {
    // simple blink for now
    if(!check_time(50))
        return;
    if(leds[0] == color)
        turn_all_off();
    else
        turn_all_on(color);
}

void Strip::round_animation(CRGB color, short direction) {
    int start = 0;
    int end = num_leds;
    int middle = num_leds / 2;
    if (direction > 0)
        end = middle + 1;
    else
        start = middle;
    if(!check_time(500))
        return;
    if(leds[middle] == color)
        turn_all_off();
    else
        turn_on(color, start, end);
}
