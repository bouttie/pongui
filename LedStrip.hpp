#ifndef LEDSTRIP_H
#define LEDSTRIP_H

#include <Arduino.h>
#include <FastLED.h>

#define LED_TYPE    WS2812B
#define COLOR_ORDER GRB

class Strip {
    private:
        byte brightness;
        unsigned long current_time;
        unsigned long previous_time;
        bool check_time(int ms_delay);

    public:
        CRGB *leds;
        byte num_leds;
        template <uint8_t DATA_PIN> void init(byte num_leds) {
            this->leds = (CRGB *)malloc(num_leds);
            this->num_leds = num_leds;
            FastLED.addLeds<LED_TYPE, DATA_PIN, COLOR_ORDER>(this->leds, num_leds);
        };
        void turn_on(CRGB color, int start, int end);
        void turn_all_off();
        void turn_all_on(CRGB color);
        void set_random_color_all();
        void set_random_color_led(int i);
        void set_christmas_color_led(int i);
        void set_brightness(int value);
        void win_animation(CRGB color);
        void round_animation(CRGB color, short direction);
};

#endif
