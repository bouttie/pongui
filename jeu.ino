#include "LedStrip.hpp"
#include "Player.hpp"
#include <FastLED.h>

#define LED_PIN     5
#define NUM_LEDS    150

// Analog
# define SLIDING_SWITCH_PIN 2

// Digital
# define BUTTON_RIGHT_PIN 2
# define BUTTON_LEFT_PIN 3

// Game params
#define BALL_LENGTH 10
#define WIN_ANIMATION_SECONDS 2
#define ROUND_ANIMATION_SECONDS 2
#define DELAY 20
#define POINTS_TO_WIN 3
#define HITTING_RANGE 10
#define SPECIAL_EFFECT_SPEED 4

// Speed multiplier treshold mapping for late attacks
float multipliers[HITTING_RANGE] = {1, 1, 1, 1, 1.25, 1.5, 2, 2.5, 3, SPECIAL_EFFECT_SPEED};

int val = 0;
bool playing = false;
byte ball_position = 0;
float speed = 1;
unsigned long animation_start_time;

Strip strip = Strip();
Player player_left = Player(BUTTON_LEFT_PIN, CRGB(255, 0, 0), 0, HITTING_RANGE, POINTS_TO_WIN);
Player player_right = Player(BUTTON_RIGHT_PIN, CRGB(0, 0, 255), NUM_LEDS - 1, HITTING_RANGE, POINTS_TO_WIN);
Player *sender = nullptr;
Player *receiver = nullptr;
Player *scorer = nullptr;

void setup() {
    delay(3000); // power-up safety delay
    Serial.begin(9600);
    pinMode(BUTTON_RIGHT_PIN, INPUT_PULLUP);
    pinMode(BUTTON_LEFT_PIN, INPUT_PULLUP);
    strip.init<LED_PIN>(NUM_LEDS);
    strip.set_random_color_all();
    FastLED.show();
}

void loop() {
    FastLED.show();

    set_brightness_from_switch();
    if(!playing) {
        if(scorer) {
            animate_win();
            return;
        }
        animate_idle();
        if(player_left.pressing()) {
            sender = &player_left;
            receiver = &player_right;
        } else if(player_right.pressing()) {
            sender = &player_right;
            receiver = &player_left;
        }
        if(sender) {
            playing = true;
            strip.turn_all_off();
            ball_position = sender->position;
            // Trigger round animation at startup
            animation_start_time = millis();
            scorer = sender;
        }
    } else {
        if (scorer) {
            animate_round();
        } else {
            display_hitting_zone_dot();
            play();
        }
    }
}



unsigned long pm = 0;
unsigned long cm = 0;

void play() {
    int hit_score = receiver->hitting(ball_position);
    if(hit_score > 0) {
        if(speed == SPECIAL_EFFECT_SPEED)
            strip.turn_all_off();
        speed = multipliers[hit_score];
        switch_player();
    }

    cm = millis();
    // Ensure each movement takes more than DELAY ms
    if(cm - pm < DELAY / speed)
        return;
    pm = cm;

    if(!is_in_range(ball_position)) {
        // The sender won this round
        scorer = sender;
        scorer->score();
        speed = 1;
        animation_start_time = cm;
        strip.turn_all_off();

        if (scorer->won) {
            // Use the win animation for the winner
            return reset();
        }

        // More rounds are needed to win the game
        ball_position = scorer->position;
        return;
    }

    strip.leds[ball_position] = sender->color;
    ball_position += sender->direction;

    int turn_black = ball_position - BALL_LENGTH * sender->direction;
    if(is_in_range(turn_black) && speed < SPECIAL_EFFECT_SPEED)
        strip.leds[turn_black] = CRGB(0, 0, 0);
}

void reset() {
    playing = false;
    sender->init();
    receiver->init();
    sender = nullptr;
    receiver = nullptr;
}

void switch_player() {
    Player *temp = sender;
    sender = receiver;
    receiver = temp;
}


bool is_in_range(int i) {
    return i < NUM_LEDS && i >= 0;
}

void set_brightness_from_switch() {
    val = analogRead(SLIDING_SWITCH_PIN);
    strip.set_brightness(val);
}

void animate_idle() {
    int i = random(NUM_LEDS - 1);
    strip.set_random_color_led((i + 1) % NUM_LEDS);
}

void animate_win() {
    if((millis() - animation_start_time) / 1000 > WIN_ANIMATION_SECONDS) {
        scorer = nullptr;
        strip.turn_all_off();
        return;
    }
    strip.win_animation(scorer->color);
}

void animate_round() {
if((millis() - animation_start_time) / 1000 > ROUND_ANIMATION_SECONDS) {
        scorer = nullptr;
        strip.turn_all_off();
        return;
    }
    strip.round_animation(scorer->color, scorer->direction);
}

void display_hitting_zone_dot() {
    strip.leds[player_left.position + player_left.hitting_range] = player_left.color;
    strip.leds[player_right.position - player_right.hitting_range] = player_right.color;
}
