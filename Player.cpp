#include "Player.hpp"


Player::Player(byte button_pin, CRGB color, byte position, byte hitting_range, int points_to_win) {
    this->button_pin = button_pin;
    this->color = color;
    this->position = position;
    this->hitting_range = hitting_range;
    // automagic direction, could be improved
    this->direction = position == 0 ? 1 : -1;
    this->points_to_win = points_to_win;
    this->init();
}

void Player::init() {
    this->points = 0;
    this->won = false;
}

bool Player::pressing() {
    int value = digitalRead(button_pin);

    // Ensure longpressing accounts only for one press
    if(button_state == value)
        return false;

    button_state = value;
    return value == LOW;
}


int Player::hitting(byte ball_position) {
    // Returns the hitting score if the user press at right time
    // Return a negative number otherwise
    if (!pressing())
        return -1;
    return hitting_range - abs(ball_position - position) - 1;
}

void Player::score() {
    this->points ++;
    this->won = this->points >= this->points_to_win;
}
